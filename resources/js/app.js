import Vue from 'vue'

import VueRouter from 'vue-router'

Vue.use(VueRouter)


require('./bootstrap');

window.Vue = require('vue').default;

// toaster Notification
import Toaster from 'v-toaster'

import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster, {timeout: 5000})



import Home from './pages/Home.vue'
import header from './components/Header.vue'
Vue.component('my-header', header)

import categoryList from './pages/category/Index.vue'
import createCategory from './pages/category/create.vue'
import editCategory from './pages/category/edit.vue'

const router = new VueRouter({

    mode: 'history',

    routes: [

        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/category',
            name: 'category',
            component: categoryList
        },
        {
            path: '/category/create',
            name: 'categorycreate',
            component: createCategory
        },
        {
            path: '/category/edit/:slug',
            name: 'categoryEdit',
            component: editCategory
        }
    ],

});

const app = new Vue({
    el: '#app',
    router
});
